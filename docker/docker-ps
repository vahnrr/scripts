#!/usr/bin/env sh
# Uses: docker grep head
#
# Context: I found the 'docker ps' filtering syntax sometimes unreliable and
# limited. That's where good old 'grep' helped, it can handle regexes! Also, I
# often used to look for a list of running containers, a simple OR regex worked,
# but it became a repetitive to write one everytime. That's why passing a list
# of arguments to this script builds an OR regex that's sent to 'grep'.
#
# $@ (optional): Match any of the arguments provided (OR regex).
# $1 (optional): Regex to filter with.
filter="${1}"

if [ "$1" = '-h' ] || [ "$1" = '--help' ]; then
  script_name="${0##*/}"
  printf "\
Run either a vanilla 'docker ps' or a regex-filtered version.

Usage: %s
       %s [REGEX_FILTER]
       %s [MATCH]...\n" \
      "${script_name}" "${script_name}" "${script_name}"
  exit 1
fi

# When no filter was provided, run a vanilla 'docker ps'.
if [ -z "${filter}" ]; then
  docker ps

else
  # Multiple arguments were provided, match against any of one them.
  if [ $# -gt 1 ]; then
    # The first argument is already included in the 'filter' variable.
    shift

    # Compile a '|' separated list of valid names to filter in.
    #
    # It contains a list and should be expanded for the loop to take place.
    # shellcheck disable=SC2068
    for argument in $@; do
      filter="${filter}|${argument}"
    done

    # Make the filter into an OR regex.
    filter="(${filter})"
  fi

  # 1. 'head' slurps the 'docker ps' header
  # 2. 'grep' then match the regex against the list of containers
  # 3. Finally both the header and the matches are displayed
  docker ps | { head -n 1; grep -E "${filter}"; }
fi
