> Collection of scripts I made over the years.

# Self imposed limitations

- Use the shell's builtins as much as possible
- Stick to POSIX-compliant Shell code and POSIX versions of utilities (thanks to `man 1P`)
- Always implement a usage message (accessible by passing `-h` or `--help`)
- Comply with [`shellcheck`](https://github.com/koalaman/shellcheck) recommendations
- Self-contained, scripts shouldn't depend on one another

# `docker/` scripts

I started learning Go after wrapping my head around the [awesome talk _Containers from Scratch_ from Liz Rice](https://www.youtube.com/watch?v=8fi7uSYlOdc).
Which led me to Docker that I love using.
However its CLI lacked some interactivity/features I imagined.
So I decided to implement them myself as scripts.

Example of `docker-image-shell-picker`:

![docker-image-shell-picker](https://gitlab.com/vahnrr/scripts/uploads/6157eb0ccd398b67b9e9607a475fb59d/docker-image-shell-picker.gif)

# `sourced/` scripts

Since those affect the shell that calls them, they should be sourced.
I tend to dynamically create aliases to do that:

``` shell
for script in ~/.local/bin/sourced/*; do
  # Only create aliases to executable scripts.
  [ -x "${script}" ] && alias "${script##*/}"=". ${script}"
done
```

# `wsl/` scripts

As I had the _opportunity_ to work on a Windows device, some scripts had to be adapted.

