#!/usr/bin/env sh
# Uses: cd find fzf

if [ "${1}" = '-h' ] || [ "${1}" = '--help' ]; then
  printf "\
Navigate to a subdirectory by fuzzily picking it from a list.

Usage: . ./cd-picker\n"
  return 1
fi

# Find sub-directories (even through symlinks) but exclude the current one from
# the results otherwise '.' becomes an available entry in the picker.
#
# NOTE: Faster alternative: fd --type=d --follow
chosen_subdirectory="$(
  find -L . ! -path . -type d | fzf --header='Navigate to which directory?'
)"

# Go into the subdirectory only if it exists, terminating 'fzf' would cause the
# variable to store an empty value which isn't a valid directory.
if [ -d "${chosen_subdirectory}" ]; then
  cd "${chosen_subdirectory}" || return 1

else
  printf 'No directory was picked.\n' >&2
  return 1
fi
